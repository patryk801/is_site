import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-stuff',
  templateUrl: './stuff.component.html',
  styleUrls: [
    './stuff.component.css',
    '../../assets/main.css'
  ]
})
export class StuffComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
