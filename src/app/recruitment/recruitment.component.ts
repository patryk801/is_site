import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-recruitment',
  templateUrl: './recruitment.component.html',
  styleUrls: [
    '../../assets/main.css',
    './recruitment.component.css',
    '../../../node_modules/bootstrap/dist/css/bootstrap.min.css']
})
export class RecruitmentComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
