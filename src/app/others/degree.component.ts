import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-degree',
  template: `
    <h1 class="no-margin">
      <p class="header-medium-font" [ngClass]="{activeGroup: activeGroupClass==dynamicClass}">{{content}}</p> 
    </h1>
  `,
  styles: ['.activeGroup { color: #93b658!important; }', 'p {color: white}'],
  styleUrls: [
    '../subjects/subjects.component.css',
    '../../assets/main.css'
  ]
})
export class DegreeComponent implements OnInit {

  @Input() content = "I stopień"
  @Input() dynamicClass = "bachelor"
  @Input() activeGroupClass = "bachelor"
  constructor() { }

  ngOnInit() {
  }

}
